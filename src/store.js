import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

 Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    socios: []
  },

  getters: {
    getSocios: state => state.socios,
    getSociosLength: state => state.socios.length
  },

  mutations: {
    SET_SOCIOS (state, data) {
      state.socios = data
    },

    ADD_NEW_SOCIOS (state, payload) {
      state.socios.push(payload)
    },

    REMOVE_SOCIOS (state, socios, id) {
      let index = state.socios.indexOf(socios => socios.id === id)
      state.socios.splice(index, 1)
    }
  },

  actions: {
    GET_SOCIOS({commit}) {
      axios.get('http://localhost:3000/socios')
      .then(res => res.data)
      .then(data => {commit('SET_SOCIOS', data)})
    },

    ADD_SOCIOS ({commit}, payload) {
      axios.post('http://localhost:3000/socios', payload)
      .then(resp => resp.data)
      .then(payload => (commit('ADD_NEW_SOCIOS')))
    },

    DELET_SOCIOS({commit}, id) {
      const url = 'http://localhost:3000/socios/'+ id;
      axios.delete(url)
      .then(resp => resp.data)
      .then(response => (commit('REMOVE_SOCIOS')))
    }
  }
})
