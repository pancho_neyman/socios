import Vue from 'vue'
import Router from 'vue-router'

import Dashboard from '@/components/dashboard/Dashboard.vue'
import Add from '@/components/form/Add.vue'
import Edit from '@/components/edit/Edit.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Dashboard',
      component: Dashboard
    },
    {
      path: '/add',
      name: 'Add',
      component: Add
    },
    {
      path: '/edit/:id',
      name: 'edit',
      component: Edit
    }
  ]
})
